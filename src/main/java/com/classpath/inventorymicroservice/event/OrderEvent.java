package com.classpath.inventorymicroservice.event;

import com.classpath.inventorymicroservice.model.Order;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderEvent {
    private EventType eventType;
    private Order order;
    private String timeStamp;
}