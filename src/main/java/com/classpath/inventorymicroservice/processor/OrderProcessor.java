package com.classpath.inventorymicroservice.processor;

import com.classpath.inventorymicroservice.event.OrderEvent;
import com.classpath.inventorymicroservice.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderProcessor {

    @StreamListener(Sink.INPUT)
    public void processOrder(OrderEvent orderEvent){
      log.info("Proceesed the order from Order-Microservice :: {}", orderEvent);
    }
}