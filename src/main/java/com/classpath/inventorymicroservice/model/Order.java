package com.classpath.inventorymicroservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Order {
    private long orderId;
    private String name;
    private LocalDate orderDate;
    private String emailAddress;
    private double orderPrice;
}